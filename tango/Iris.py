############################################################################
# This file is part of LImA, a Library for Image Acquisition
#
# Copyright (C) : 2009-2024
# European Synchrotron Radiation Facility
# CS40220 38043 Grenoble Cedex 9
# FRANCE
#
# Contact: lima@esrf.fr
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
############################################################################
#=============================================================================
#
# file :        Iris.py
#
# description : Python source for the Iris and its commands.
#                The class is derived from Device. It represents the
#                CORBA servant object which will be accessed from the
#                network. All commands which can be executed on the
#                Pilatus are implemented in this file.
#
# project :     TANGO Device Server
#
# copyleft :    European Synchrotron Radiation Facility
#               BP 220, Grenoble 38043
#               FRANCE
#
#=============================================================================
#         (c) - Bliss - ESRF
#=============================================================================
#
import PyTango
from Lima import Core
from Lima import Iris as IrisAcq
from Lima.Server import AttrHelper


class Iris(PyTango.LatestDeviceImpl):
    Core.DEB_CLASS(Core.DebModApplication, 'LimaCCDs')

#------------------------------------------------------------------
#    Device constructor
#------------------------------------------------------------------
    @Core.DEB_MEMBER_FUNCT
    def __init__(self,*args) :
        # self.__Attribute2FunctionBase = {'temperature': 'Temperature'}
        PyTango.Device_4Impl.__init__(self,*args)
        # self.set_state(PyTango.DevState.ON)
        self.init_device()

#------------------------------------------------------------------
#    Device destructor
#------------------------------------------------------------------
    def delete_device(self):
        pass

#------------------------------------------------------------------
#    Device initialization
#------------------------------------------------------------------
    @Core.DEB_MEMBER_FUNCT
    def init_device(self):
        self.set_state(PyTango.DevState.ON)
        self.get_device_properties(self.get_device_class())


#==================================================================
#
#    Iris read/write attribute methods
#
#==================================================================
    def __getattr__(self, name):
        return AttrHelper.get_attr_4u(self, name, _IrisCam)


#==================================================================
#
#    IrisClass class definition
#
#==================================================================
class IrisClass(PyTango.DeviceClass):

    class_property_list = {}

    device_property_list = {
        # define one and only one of the following 4 properties:
        'camera_name':
        [PyTango.DevString,
         "Camera name", None],

         'serial_number':
        [PyTango.DevString,
         "Camera serial number", None]
        }

    attr_list = {
        'temperature':
        [[PyTango.DevDouble, PyTango.SCALAR, PyTango.READ],
        {
            'label': 'Temperature',
            'unit': 'C',
            'format': '%f',
            'description': 'Sensor temp in Celsius'
        }],
        'readout_time':
        [[PyTango.DevDouble, PyTango.SCALAR, PyTango.READ],
        {
            'label': 'Readout time',
            'unit': 'us',
            'format': '%f',
            'description': 'Sensor readouttime in us'
        }],
        'acq_timeout':
        [[PyTango.DevDouble, PyTango.SCALAR, PyTango.READ_WRITE],
        {
            'label': 'Acquisition timeout',
            'unit': 's',
            'format': '%f',
            'description': 'Acquisition timeout in s'
        }]
    }

    def __init__(self,name) :
        PyTango.DeviceClass.__init__(self,name)
        self.set_type(name)

#----------------------------------------------------------------------------
# Plugins
#----------------------------------------------------------------------------

_IrisCam = None
_IrisInterface = None

def get_control(camera_name="", serial_number="", **keys):
    global _IrisCam 
    global _IrisInterface
    if _IrisCam is None:
        _IrisCam = IrisAcq.Camera(camera_name, serial_number)
        _IrisInterface = IrisAcq.Interface(_IrisCam)

    control = Core.CtControl(_IrisInterface)

    return control

def get_tango_specific_class_n_device():
    return IrisClass,Iris
