###########################################################################
# This file is part of LImA, a Library for Image Acquisition
#
#  Copyright (C) : 2009-2017
#  European Synchrotron Radiation Facility
#  CS40220 38043 Grenoble Cedex 9
#  FRANCE
#
#  Contact: lima@esrf.fr
#
#  This is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FIMODESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
############################################################################

set(PVCAM_SDK_INCLUDE_DIR)
set(PVCAM_SDK_LIBRARIES)
set(PVCAM_SDK_DEFINITIONS)

find_path(PVCAM_SDK_INCLUDE_DIR
    NAMES pvcam.h
    DOC "PvCam SDK include directory")
find_library(PVCAM_SDK_LIB_PVCAM
    NAMES pvcam pvcam64
    DOC "PvCam SDK include directory")
find_library(PVCAM_SDK_LIB_HTRACK
    NAMES pvcam_helper_track pvcam_helper_track_v1
    DOC "PvCam SDK include directory")
find_library(PVCAM_SDK_LIB_HCOLOR
    NAMES pvcam_helper_color pvcam_helper_color_v2
    DOC "PvCam SDK include directory")

set(PVCAM_SDK_LIBRARIES
    ${PVCAM_SDK_LIB_PVCAM} ${PVCAM_SDK_LIB_HTRACK} ${PVCAM_SDK_LIB_HCOLOR}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PvCam
    DEFAULT_MSG
    PVCAM_SDK_LIB_PVCAM
    PVCAM_SDK_LIB_HTRACK
    PVCAM_SDK_LIB_HCOLOR
    PVCAM_SDK_INCLUDE_DIR)

if(PvCam_FOUND)
    if(NOT TARGET PvCam::PvCam)
        add_library(PvCam::PvCam INTERFACE IMPORTED)
    endif()
    if(PVCAM_SDK_INCLUDE_DIR)
        set_target_properties(PvCam::PvCam PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${PVCAM_SDK_INCLUDE_DIR}")
    endif()
    if(PVCAM_SDK_DEFINITIONS)
        set_target_properties(PvCam::PvCam PROPERTIES
            INTERFACE_COMPILE_DEFINITIONS "${PVCAM_SDK_DEFINITIONS}")
    endif()
    foreach(LIB IN LISTS PVCAM_SDK_LIBRARIES)
        get_filename_component(MOD ${LIB} NAME_WE)
        add_library(PvCam::${MOD} SHARED IMPORTED)
        set_target_properties(PvCam::${MOD} PROPERTIES
            IMPORTED_LINK_INTERFACE_LANGUAGES "C"
            IMPORTED_IMPLIB "${LIB}"
            IMPORTED_LOCATION "${LIB}")
        target_link_libraries(PvCam::PvCam INTERFACE PvCam::${MOD})
    endforeach()
endif()
