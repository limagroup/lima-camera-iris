//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#if !defined(LIMA_IRIS_CAMERA_H)
#define LIMA_IRIS_CAMERA_H

#include <atomic>
#include <string>
#include <vector>

#include <lima/Debug.h>
#include <lima/HwBufferMgr.h>
#include "lima/HwInterface.h"
#include "iris/Common.h"

#include <iris_export.h>

namespace lima {
  namespace Iris {
  class LIMA_IRIS_EXPORT Camera {
    DEB_CLASS_NAMESPC(DebModCamera, "Camera", "Iris");

    friend class Interface;
    friend class SyncCtrlObj;

    public:
      enum Status {
        Ready, Exposure, Readout, Latency, Fault
      };

      Camera(std::string camera_name, std::string serial_number);
      Camera(Camera const &) = delete;
      ~Camera();

      void prepareAcq();
      void startAcq();
      void stopAcq();
      void reset();

      int getNbAcquiredFrames();

      void getStatus(Camera::Status& status);

      void setNbFrames(int nb_frames);
      void getNbFrames(int &nb_frames) const;
      void getNbHwAcquiredFrames(int &nb_acq_frames) const;

      void setExpTime(double exp_time);
      void getExpTime(double& exp_time) const;
      void getExposureTimeRange(double& min_expo, double& max_expo) const;

      void setAcqTimeout(double acq_timeout);
      void getAcqTimeout(double& acq_timeout) const;

      void setGain(double gain);
      void getGain(double& gain) const;

      std::string getDetectorModel() { return m_detector_model; };

      void setLatTime(double lat_time);
      void getLatTime(double &lat_time) const;
      void getLatTimeRange(double& min_expo, double& max_expo) const;

      void setTrigMode(TrigMode mode);
      void getTrigMode(TrigMode& mode) const;

      void setBin(const Bin &);
      void getBin(Bin&) const;
      void checkBin(Bin &aBin);

      // -- detector info object
      void getImageType(ImageType& type) const;
      void setImageType(ImageType type);

      void getDetectorType(std::string& type) const;
      void getDetectorModel(std::string& model) const;
      void getDetectorImageSize(Size& size) const;

      void getTemperature(double& temp) const;

      void setRoi(const Roi& set_roi);
      void getRoi(Roi& hw_roi) const;
      void checkRoi(const Roi& set_roi, Roi& hw_roi);

      void getFrameRate(double& frame_rate) const;

      void getPixelSize(double& x_pixel_size, double& y_pixel_size) const;

      void getReadoutTime(double& readout) const;

      // -- Buffer control object
      HwBufferCtrlObj *getBufferCtrlObj() { return &m_buffer_ctrl_obj; }

    private:
      class _AcqThread;
      friend class _AcqThread;
      double m_exp_time;
      double m_lat_time;
      double m_acq_timeout;
      int m_nb_frames;
      double m_gain;
      std::atomic_int m_image_number;
      bool m_acq_started;
      std::string m_detector_type;
      std::string m_detector_model;
      std::atomic_bool m_quit;
      std::atomic_bool m_wait_flag;
      std::atomic_bool m_thread_running;
      Camera::Status m_status;
      void _setStatus(Camera::Status, bool force);
      void _stopAcq();
      void _startAcq();
      void _getDetectorType();
      std::vector<uns8> m_circular_buffer;
      uns32 m_readoutTime;

      // helpers
      std::map<TrigMode, PL_EXPOSURE_MODES> trigModesMap {
        {IntTrig, EXT_TRIG_INTERNAL},
        //{IntTrigMult, EXT_TRIG_SOFTWARE_EDGE},
        {ExtTrigSingle, EXT_TRIG_TRIG_FIRST},
        {ExtTrigMult, EXT_TRIG_EDGE_RISING},
        //{ExtGate, EXT_TRIG_LEVEL}
      };
      void _printError(const std::string message="") const;

      // lima stuff
      SoftBufferCtrlObj m_buffer_ctrl_obj;

      // PV stuff
      char cam_name[255];
      std::vector<CameraContext*> contexts;
      CameraContext *ctx = nullptr;
      short	m_cam;
      TrigMode m_trigger_mode;
      _AcqThread *m_acq_thread = nullptr;
      Cond m_cond;
    };

  } // namespace Iris
} // namespace lima

#endif // LIMA_IRIS_CAMERA_H
