#ifndef IRISROICTRLOBJ_H
#define IRISROICTRLOBJ_H

#include <iris_export.h>

#include "lima/HwRoiCtrlObj.h"
#include "lima/HwInterface.h"

namespace lima {
    namespace Iris {
        class Camera;

        class RoiCtrlObj : public HwRoiCtrlObj {
            DEB_CLASS_NAMESPC(DebModCamera, "RoiCtrlObj", "Iris");

            public:
                RoiCtrlObj(Camera&);
                virtual ~RoiCtrlObj();

                virtual void setRoi(const Roi& set_roi);
                virtual void getRoi(Roi& hw_roi);
                virtual void checkRoi(const Roi& set_roi, Roi& hw_roi);

            private:
                Camera& m_cam;
        };
    }
}

#endif // IRISROICTRLOBJ_H
