//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2022
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "iris/Camera.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <cstdlib>

using namespace lima;
using namespace lima::Iris;


//---------------------------
//- Thread helper class
//---------------------------

class Camera::_AcqThread : public Thread
{
    DEB_CLASS_NAMESPC(DebModCamera, "Camera", "_AcqThread");
    public:
      _AcqThread(Camera &aCam);
      virtual ~_AcqThread();
    
    protected:
        virtual void threadFunction();
    
    private:
        Camera& m_cam;
};

Camera::_AcqThread::_AcqThread(Camera &aCam) : m_cam(aCam)
{
    DEB_CONSTRUCTOR();

    pthread_attr_setscope(&m_thread_attr, PTHREAD_SCOPE_PROCESS);
}

Camera::_AcqThread::~_AcqThread()
{
    DEB_DESTRUCTOR();

    {
      AutoMutex aLock(m_cam.m_cond.mutex());
      m_cam.m_quit = true;
      m_cam.m_cond.broadcast();
    }
   
    join();
}


//---------------------------
//- Ctor
//---------------------------
Camera::Camera(std::string camera_name, std::string serial_number) 
: 
  m_cam(-1),
  m_nb_frames(1),
  m_image_number(0),
  m_exp_time(1.),
  m_lat_time(0.),
  m_acq_timeout(10.0),
  m_wait_flag(true),
  m_status(Ready),
  m_thread_running(true),
  m_quit(false)
{
  DEB_CONSTRUCTOR();

  m_lat_time = 1.0;

  int camera_index = -1;

  if(PV_OK != InitPVCAM(contexts)) {
    _printError("Cannot init pvcam");
  }

  if (contexts.empty())
    THROW_HW_ERROR(Error) << "No camera found";

  for (int i = 0; i < contexts.size(); i++) {
    CameraContext* ctx  = contexts[i];
    if (!camera_name.empty() && camera_name.compare(ctx->camName) == 0) {
      camera_index = i;
      break;
    } else if (!serial_number.empty()) {
      char cam_serial_number[MAX_ALPHA_SER_NUM_LEN];
      try {
        if (PV_OK != OpenCamera(ctx))
          {
              PrintErrorMessage(pl_error_code(), "pl_cam_open() error");
              continue;
          }
        if (IsParamAvailable(ctx->hcam, PARAM_HEAD_SER_NUM_ALPHA, "PARAM_HEAD_SER_NUM_ALPHA")) {
          if (PV_OK != pl_get_param(ctx->hcam, PARAM_HEAD_SER_NUM_ALPHA, ATTR_CURRENT, (void*)&cam_serial_number))
          {
              PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_HEAD_SER_NUM_ALPHA) error");
              THROW_HW_ERROR(Error) << "Error getting serial number";
          }
        } else {
          THROW_HW_ERROR(Error) << "Serial number not available";
        }
      } catch (std::invalid_argument& e) {
        DEB_WARNING() << "Invalid arguments " << e.what();   
        CloseCamera(ctx);
        continue;
      }

      if (serial_number.compare(cam_serial_number) == 0) {
        camera_index = i;
        break;
      }      
    }
  }

  if (camera_index == -1)
    THROW_HW_ERROR(Error) << "No camera found with SN" << serial_number;

  CameraContext* _ctx = contexts[camera_index];

  // Open first found camera
  if (!OpenCamera(_ctx))
    _printError("Cannot open selected camera");

  ctx = _ctx;

  // register callbacks if EOF event arrives.
  if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF, (void*) GenericEofHandler, ctx)) {
    CloseAllCamerasAndUninit(contexts);
  }

  _getDetectorType();
  m_detector_model = ctx->camName;

  // set roi for full frame
  Roi fullFrame(0, 0, ctx->sensorResX, ctx->sensorResY);
  setRoi(fullFrame);

  // set binning to default 1x1
  setBin(Bin(1,1));

  // set readout_time to maximum
  m_readoutTime = 30368; // us

  setTrigMode(IntTrig);
  
  double min_exp, max_exp;
  getExposureTimeRange(min_exp, max_exp);
  double exp_time = std::min(1.0, max_exp);
  setExpTime(exp_time);
  
  m_acq_thread = new _AcqThread(*this);
  if (m_acq_thread)
    m_acq_thread->start();
  else
    THROW_HW_ERROR(Error) << "Failed to construct AcqThread" << serial_number;
}

//---------------------------
//- Dtor
//---------------------------
Camera::~Camera()
{
  DEB_DESTRUCTOR();
  
  for (CameraContext* ctx : contexts)
  {
      if (!ctx || !ctx->isCamOpen)
          continue;

      {
          std::unique_lock<std::mutex> lock(ctx->eofEvent.mutex);
          if (ctx->threadAbortFlag)
              continue;
          ctx->threadAbortFlag = true;
          DEB_TRACE() << "Requesting ABORT on camera " << ctx->hcam;
      }
      ctx->eofEvent.cond.notify_all();
  }
 
  // _AcqThread keeps a refence to the camera, need to join
  if (m_acq_thread)
    delete m_acq_thread;

  CloseAllCamerasAndUninit(contexts);
}

void Camera::_AcqThread::threadFunction() {
  DEB_MEMBER_FUNCT();

  struct sched_param param;
  param.sched_priority = 10;
  int res;
  if (res = pthread_setschedparam(pthread_self(), SCHED_FIFO, &param)) {
    DEB_WARNING() << "Could not set Fifo scheduling for Acquisition thread - Error #" << res;
  }

  StdBufferCbMgr& buffer_mgr = m_cam.m_buffer_ctrl_obj.getBuffer();

  while (!m_cam.m_quit) {
    {
      AutoMutex aLock(m_cam.m_cond.mutex());
    
      while(m_cam.m_wait_flag && !m_cam.m_quit) {
        DEB_TRACE() << "Wait";
        m_cam.m_thread_running = false;
        m_cam.m_cond.broadcast();
        m_cam.m_cond.wait();
      }
      
      if (m_cam.m_quit)
        return;
      else
        m_cam.m_thread_running = true;
    }

    m_cam._setStatus(Camera::Exposure, false);

    bool continueAcq = true;

    md_frame *mdFrameInMemory;
    if (PV_OK != pl_md_create_frame_struct_cont(&mdFrameInMemory, 1)) {
      PrintErrorMessage(pl_error_code(), "pl_md_create_frame_struct_cont(cRoiCount) error");
      m_cam._setStatus(Camera::Fault, false);
      continueAcq = false;
    }
    
    m_cam._setStatus(Camera::Readout, false);
    while (continueAcq && (!m_cam.m_nb_frames || m_cam.m_image_number < m_cam.m_nb_frames)) {
      eof_event evt = WaitForEofEvent(m_cam.ctx, uns32(m_cam.m_acq_timeout * 1000));
      if (evt != eof_event::success) {
        // Timeout or abort acquisition
        if (evt == eof_event::timeout) {
          // Timeout
          DEB_ERROR() << "Wait for end of frame Timeout";
          m_cam._setStatus(Camera::Fault, false);          
        }
        continueAcq = false;
      } else {
        while (!m_cam.ctx->eofFrame.empty() && (!m_cam.m_nb_frames || m_cam.m_image_number < m_cam.m_nb_frames))
        {
          // Pop frame pointer from the FIFO
          void* eofFrame = m_cam.ctx->eofFrame.pop();
          
          // Decode the frame and extract metadata structures
          if (PV_OK != pl_md_frame_decode(mdFrameInMemory, eofFrame, m_cam.ctx->exposureBytes)) {
            PrintErrorMessage(pl_error_code(), "pl_md_frame_decode() error");
          }

          if (mdFrameInMemory->header) {
            time_t rawtime;
            struct tm *timeinfo;

            time(&rawtime);
            timeinfo = localtime(&rawtime);
            char timeinfo_sz[32];
            strftime(timeinfo_sz, 32, "%d-%b %H:%M:%S", timeinfo);

            if (mdFrameInMemory->header->frameNr - m_cam.ctx->frameNr > 1)
              DEB_ERROR() << "Frame dropped " << mdFrameInMemory->header->frameNr - 1;

            m_cam.ctx->frameNr = mdFrameInMemory->header->frameNr;

            if (mdFrameInMemory->header->version == 3) {
              md_frame_header_v3 *header = reinterpret_cast<md_frame_header_v3 *>(mdFrameInMemory->header);
              DEB_TRACE() << "frameNr: " << header->frameNr
                  << " timestampBOF: " << header->timestampBOF
                  << " timestampEOF: " << header->timestampEOF;
            } else {
              md_frame_header *header = mdFrameInMemory->header;
              DEB_TRACE() << "frameNr: " << header->frameNr << " timestampBOF: " << header->timestampBOF
                          << " timestampEOF: " << header->timestampEOF;
            }
          }

          if (mdFrameInMemory->roiCount != 1) {
            DEB_ERROR() << "Required 1 and received " << mdFrameInMemory->roiCount << " ROI count do not match";
            break;
          }

          if (mdFrameInMemory->extMdData)
            PrintMetaExtMd(mdFrameInMemory->extMdData, mdFrameInMemory->extMdDataSize);

          HwFrameInfoType frame_info;
          frame_info.acq_frame_nb = m_cam.m_image_number;
          void *dstPtr = buffer_mgr.getFrameBufferPtr(m_cam.m_image_number);
          void *srcPtr = mdFrameInMemory->roiArray[0].data;
          uns32 srcSize = mdFrameInMemory->roiArray[0].dataSize;
          const FrameDim &fDim = buffer_mgr.getFrameDim();
          
          if (srcSize != fDim.getMemSize()) {
            DEB_ERROR() << "Required src (" << srcSize << ") and FrameDim size (" << fDim.getMemSize()
                        << ") do not match\n";
            break;
          }

          memcpy(dstPtr, srcPtr, srcSize);

          continueAcq = buffer_mgr.newFrameReady(frame_info);
            
          m_cam.m_image_number++;
        }
      }
    } // end acquisition while

    DEB_TRACE() << "Acquisition thread loop ends with " << m_cam.m_image_number << " frames";

    // Cleanup before exiting the program.
    if (PV_OK != pl_md_release_frame_struct(mdFrameInMemory)) {
      PrintErrorMessage(pl_error_code(), "pl_md_release_frame_struct() error");
    }

    // Once we have acquired the desired number of frames, the continuous acquisition should be
    // stopped with pl_exp_stop_cont function
    if (PV_OK != pl_exp_stop_cont(m_cam.ctx->hcam, CCS_HALT)) {
      m_cam._printError();
    }

    m_cam._setStatus(Camera::Ready, false);

    // wait for the next acquistion
    m_cam.m_wait_flag = true;
  }
}

void Camera::reset() {
  DEB_MEMBER_FUNCT();
  try {
    _stopAcq();
  } catch(Exception e) {
    THROW_HW_ERROR(Error) << e;
  }
}

void Camera::_getDetectorType() {
  DEB_MEMBER_FUNCT();

   char systemName[MAX_SYSTEM_NAME_LEN];

   if (IsParamAvailable(ctx->hcam, PARAM_SYSTEM_NAME, "PARAM_SYSTEM_NAME"))
     if (PV_OK != pl_get_param(ctx->hcam, PARAM_SYSTEM_NAME, ATTR_CURRENT, (void*)systemName))
     {
         _printError("Error getting chip name");
     } else {
        m_detector_type = systemName;
    }
}

void Camera::prepareAcq()
{
  DEB_MEMBER_FUNCT();
  m_image_number       = 0;
  m_acq_started        = false;
  ctx->threadAbortFlag = false;
  ctx->frameNr         = 0;

  const uns32 exposureTime = m_exp_time * 1000.; // seconds -> miliseconds
  int32 trigMode           = trigModesMap[m_trigger_mode];
  const int16 bufferMode   = CIRC_OVERWRITE;
  rgn_type roi = ctx->region;

  if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &roi, trigMode, exposureTime, &ctx->exposureBytes, bufferMode)) {
    _setStatus(Camera::Fault, false);
    _printError("Acquisition (pl_exp_setup_cont()) setup error");
  }
  DEB_TRACE() << "Acquisition setup successful ";

  UpdateCtxImageFormat(ctx);

  // A circular buffer of 32 frames is far enough
  const int nb_frames_circular_buffer = 32;
  std::size_t circular_buffer_size    = nb_frames_circular_buffer * ctx->exposureBytes;
  DEB_TRACE() << "Frame bytes = " << ctx->exposureBytes;
  m_circular_buffer.resize(circular_buffer_size);

  // get readout time for current acquisition
  if (IsParamAvailable(ctx->hcam, PARAM_READOUT_TIME, "PARAM_READOUT_TIME")) {
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_READOUT_TIME, ATTR_CURRENT, (void *)&m_readoutTime)) {
      _printError();
    }
  }
}

void Camera::_startAcq() {
  DEB_MEMBER_FUNCT();
  AutoMutex aLock(m_cond.mutex());

  m_wait_flag = false;
  m_cond.broadcast();
  m_acq_started = true;
}

void Camera::startAcq() {
  DEB_MEMBER_FUNCT();

  if (!m_acq_started) {
    if (PV_OK != pl_exp_start_cont(ctx->hcam, m_circular_buffer.data(), m_circular_buffer.size())) {
      _printError("Unable to start acq");
      _setStatus(Camera::Fault, false);
    }
    m_buffer_ctrl_obj.getBuffer().setStartTimestamp(Timestamp::now());
    // Now start the Acq. thread loop
    _startAcq();
  }
}

void Camera::stopAcq() {
  DEB_MEMBER_FUNCT();
  _stopAcq();
}

void Camera::_stopAcq() {
  DEB_MEMBER_FUNCT();

  try {
    Camera::Status status;
    getStatus(status);

    if (status != Camera::Ready) {
      DEB_TRACE() << "Stop acq";
      if (PV_OK !=  pl_exp_stop_cont(ctx->hcam, CCS_HALT)) {
        _printError();
      } else {
        {
          std::unique_lock<std::mutex> lock(ctx->eofEvent.mutex);
          ctx->threadAbortFlag = true;
        } 
        ctx->eofEvent.cond.notify_all();
        m_wait_flag = true;
        _setStatus(Camera::Ready, false);
      }
    }
  }
  catch(Exception e) {
    THROW_HW_ERROR(Error) << e;
  }
}

//-----------------------------------------------------
//
//-----------------------------------------------------

void Camera::getStatus(Camera::Status& status) {
  DEB_MEMBER_FUNCT();

  AutoMutex aLock(m_cond.mutex());
  status = m_status;

  DEB_RETURN() << DEB_VAR1(status);
}

void Camera::_setStatus(Camera::Status status, bool force) {
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(status);
  AutoMutex aLock(m_cond.mutex());

  if (force || m_status != Camera::Fault) {
    m_status = status;
  }

  m_cond.broadcast();
}

void Camera::setExpTime(double exp_time) {
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(exp_time);
  ulong64 exposure_time = (ulong64) exp_time * 1000; // convert to miliseconds

  uns16 isAccessible;
  ulong64 exposureTimeParam;
  if (PV_OK == pl_get_param(ctx->hcam, PARAM_EXPOSURE_TIME, ATTR_ACCESS, (void*)&isAccessible))
    {
      exposureTimeParam = PARAM_EXPOSURE_TIME;
    } else if (PV_OK == pl_get_param(ctx->hcam, PARAM_EXP_TIME, ATTR_ACCESS, (void*)&isAccessible)) {
      exposureTimeParam = PARAM_EXP_TIME;
    } else {
      _printError("Cannot access exposure time parameter");
    }

  // PARAM_EXPOSURE_TIME is always available, but is not settable in exposure time = VARIABLE_TIMED_MODE
  if (PL_PARAM_ACCESS::ACC_READ_WRITE == isAccessible || PL_PARAM_ACCESS::ACC_WRITE_ONLY == isAccessible) {
    if (PV_OK != pl_set_param(ctx->hcam, exposureTimeParam, (void*)&exposure_time))
      {
          _printError();
      }
  }
  m_exp_time = exp_time;
}

void Camera::getExpTime(double& exp_time) const {
  DEB_MEMBER_FUNCT();
  ulong64 exposure_time;
  uns16 isAccessible;
  ulong64 exposureTimeParam;
  if (PV_OK == pl_get_param(ctx->hcam, PARAM_EXPOSURE_TIME, ATTR_ACCESS, (void*)&isAccessible))
    {
      exposureTimeParam = PARAM_EXPOSURE_TIME;
    } else if (PV_OK == pl_get_param(ctx->hcam, PARAM_EXP_TIME, ATTR_ACCESS, (void*)&isAccessible)) {
      exposureTimeParam = PARAM_EXP_TIME;
    }

  if (PL_PARAM_ACCESS::ACC_READ_ONLY == isAccessible || PL_PARAM_ACCESS::ACC_READ_WRITE == isAccessible) {
    if (PV_OK == pl_get_param(ctx->hcam, exposureTimeParam, ATTR_CURRENT, (void*)&exposure_time))
    {
        exp_time = (double) exposure_time;
    } else {
      _printError();
    }
  }
  DEB_RETURN() << DEB_VAR1(exposure_time);
}

void Camera::setAcqTimeout(double acq_timeout) {
  DEB_MEMBER_FUNCT();
  if (acq_timeout <= m_exp_time + m_lat_time)
    THROW_HW_ERROR(Error) << "Invalid timeout value (should be larger than exp_time + lat_time)";
  m_acq_timeout = acq_timeout;
}

void Camera::getAcqTimeout(double& acq_timeout) const {
  DEB_MEMBER_FUNCT();
  acq_timeout = m_acq_timeout;
}


void Camera::getExposureTimeRange(double& min_expo, double& max_expo) const {
  DEB_MEMBER_FUNCT();
  ulong64 exposure_time_min, exposure_time_max;
  // PARAM_EXPOSURE_TIME is always available
  if (PV_OK != pl_get_param(ctx->hcam, PARAM_EXPOSURE_TIME, ATTR_MAX, (void*)&exposure_time_max)) {
    _printError();
  }

  if (PV_OK != pl_get_param(ctx->hcam, PARAM_EXPOSURE_TIME, ATTR_MIN, (void*)&exposure_time_min)) {
    _printError();
  }

  DEB_TRACE() << "Initial exposure times reloaded.";
  min_expo = (double) exposure_time_min * 1000.;
  max_expo = (double) exposure_time_max * 1000.;
}

void Camera::setNbFrames(int nb_frames) {
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(nb_frames);
  m_nb_frames = nb_frames;
}

void Camera::getNbFrames(int& nb_frames) const {
  DEB_MEMBER_FUNCT();
  nb_frames = m_nb_frames;
  DEB_RETURN() << DEB_VAR1(nb_frames);
}

void Camera::setTrigMode(TrigMode mode) {
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(mode);

  NVPC triggerModes;
  if (!ReadEnumeration(ctx->hcam, &triggerModes, PARAM_EXPOSURE_MODE, "PARAM_EXPOSURE_MODE"))
    _printError("Cannot read supported trigger modes");

  bool is_trigger_mode_supported = false;

  for (const NVP& nvp : triggerModes)
    if (nvp.value == trigModesMap[mode])
      is_trigger_mode_supported = true;

  if (!is_trigger_mode_supported)
    _printError("Trigger mode not supported");

  // this param can only be set in pl_exp_setup_cont()/pl_exp_setup_seq() function
  m_trigger_mode = mode;
}

void Camera::getTrigMode(TrigMode& mode) const {
  DEB_MEMBER_FUNCT();

  mode = m_trigger_mode;

  DEB_RETURN() << DEB_VAR1(m_trigger_mode);
}

void Camera::setGain(double gain) {
  DEB_MEMBER_FUNCT();
  // TODO: TO BE FIXED
  DEB_PARAM() << DEB_VAR1(gain);
  int16 gainMin, gainMax;

  // consider setting max/min gain on init and store in cam object.

  if (PV_OK != pl_get_param(ctx->hcam, PARAM_GAIN_INDEX, ATTR_MIN, (void*)&gainMin))
  {
      _printError();
  }

  if (PV_OK != pl_get_param(ctx->hcam, PARAM_GAIN_INDEX, ATTR_MAX, (void*)&gainMax))
  {
      _printError();
  }

  if ((static_cast<double>(gainMin) < gain) && (gain < static_cast<double>(gainMax))) {
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_GAIN_INDEX, (void*)&gain))
    {
        _printError("Gain index could not be set");
    }
    m_gain = gain;
    DEB_TRACE() << ("Setting gain index to %d\n", gain);
  } else {
    _printError("Invalid gain value");
  }
}

void Camera::getGain(double& gain) const {
  DEB_MEMBER_FUNCT();
  uns16 gainValue;
  if (IsParamAvailable(ctx->hcam, PARAM_GAIN_INDEX, "PARAM_GAIN_INDEX"))
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_GAIN_INDEX, ATTR_CURRENT, (void*)& gainValue)) {
      _printError("Could not read CCD X resolution");
    }
  gain = gainValue;
}

void Camera::getNbHwAcquiredFrames(int &nb_acq_frames) const {
  DEB_MEMBER_FUNCT();
  nb_acq_frames = m_image_number;
}

void Camera::getDetectorImageSize(Size& size) const
{
    DEB_MEMBER_FUNCT();
    if (IsParamAvailable(ctx->hcam, PARAM_SER_SIZE, "PARAM_SER_SIZE"))
      if (PV_OK != pl_get_param(ctx->hcam, PARAM_SER_SIZE, ATTR_CURRENT, (void*)& ctx->sensorResX)) {
        _printError("Could not read CCD X resolution");
      }

    if (IsParamAvailable(ctx->hcam, PARAM_PAR_SIZE, "PARAM_PAR_SIZE"))
      if (PV_OK != pl_get_param(ctx->hcam, PARAM_PAR_SIZE, ATTR_CURRENT, (void*)& ctx->sensorResY)) {
        _printError("Could not read CCD Y resolution");
      }
      
    size = Size(static_cast<int>(ctx->sensorResX), static_cast<int> (ctx->sensorResY));
}


//-----------------------------------------------------
//
//-----------------------------------------------------
void Camera::getImageType(ImageType& type) const
{
    DEB_MEMBER_FUNCT();

    enum PL_IMAGE_FORMATS imageFormat;
    if (IsParamAvailable(ctx->hcam, PARAM_IMAGE_FORMAT, "PARAM_IMAGE_FORMAT"))
      if (PV_OK != pl_get_param(ctx->hcam, PARAM_IMAGE_FORMAT, ATTR_CURRENT, &imageFormat))
        _printError("Could not read image format");

    // switch between PL_IMAGE_FORMATS enum
    switch (imageFormat)
    {
      case PL_IMAGE_FORMAT_MONO16:
      case PL_IMAGE_FORMAT_BAYER16:
          type = ImageType::Bpp16;
          break;

      case PL_IMAGE_FORMAT_MONO8:
      case PL_IMAGE_FORMAT_BAYER8:
          type = ImageType::Bpp8;
          break;

      case PL_IMAGE_FORMAT_MONO24:
      case PL_IMAGE_FORMAT_BAYER24:
      case PL_IMAGE_FORMAT_RGB24:
          type = ImageType::Bpp24;
          break;

      case PL_IMAGE_FORMAT_MONO32:
      case PL_IMAGE_FORMAT_BAYER32:
          type = ImageType::Bpp32;
          break;
    
    default:
      // values of PL_IMAGE_FORMAT_RGB48, PL_IMAGE_FORMAT_RGB72 does not seem to be supported in lima
      _printError("Unsupported Pixel Format: " +  std::to_string(imageFormat));
      break;
    }
    type = ImageType::Bpp16;
}

// // -----------------------------------------------------

// // -----------------------------------------------------
void Camera::setImageType(ImageType type)
{
    DEB_MEMBER_FUNCT();

    int32 imageType;
    switch (type)
    {
      case Bpp8:
        imageType = PL_IMAGE_FORMAT_MONO8;
      case Bpp24:
        imageType = PL_IMAGE_FORMAT_MONO24;
      case Bpp16:
        imageType = PL_IMAGE_FORMAT_MONO16;
        break;
      
      default:
        _printError("Cannot change the pixel format of the camera!");
        break;
    }

    uns16 isAccessible;

     if (PV_OK != pl_get_param(ctx->hcam, PARAM_IMAGE_FORMAT, ATTR_ACCESS, (void*)& isAccessible)) {
        _printError("Could not read image format parameter");
      } 
      if (PL_PARAM_ACCESS::ACC_READ_WRITE == isAccessible || PL_PARAM_ACCESS::ACC_WRITE_ONLY == isAccessible) {
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_IMAGE_FORMAT, ATTR_CURRENT, (void*)&imageType)) {
          _printError("Could not set image type");
        }
      }

    if (!isAccessible) {
      _printError("Cannot access image format parameter");
    }
    ctx->imageFormat = imageType;
}

void Camera::getDetectorType(std::string& type) const
{
    DEB_MEMBER_FUNCT();
    type = m_detector_type;
}

void Camera::getDetectorModel(std::string& model) const
{
    DEB_MEMBER_FUNCT();
    model = m_detector_model;   
}

void Camera::getTemperature(double& temp) const {
  DEB_MEMBER_FUNCT();
  int16 _temp;

  if (m_wait_flag && IsParamAvailable(ctx->hcam, PARAM_TEMP, "PARAM_TEMP"))
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_TEMP, ATTR_CURRENT, (void*)& _temp)) {
      _printError("Could not read temperature");
    }

  temp = _temp;
};

//-----------------------------------------------------
// Roi functions
//-----------------------------------------------------

void Camera::setRoi(const Roi& set_roi) {
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(set_roi);

  Roi r;
  Bin bin = Bin();
  Roi limaFullFrameRoi(0, 0, ctx->sensorResX, ctx->sensorResY);

  try {
    // save old roi in case of rollback
    getRoi(r);

    getBin(bin);

    rgn_type roi;
    if (limaFullFrameRoi == set_roi && !bin.isOne()) {
      std::cout << "Full frame with binning!" << std::endl;
      roi.s1 = set_roi.getTopLeft().x;
      roi.s2 = set_roi.getSize().getWidth();
      roi.sbin = bin.getX();
      roi.p1   = set_roi.getTopLeft().y;
      roi.p2   = set_roi.getSize().getHeight();
      roi.pbin = bin.getY();

    } else {
        roi.s1 = set_roi.getTopLeft().x;
        roi.s2 = set_roi.getSize().getWidth();
        roi.sbin = bin.getX();
        roi.p1 = set_roi.getTopLeft().y;
        roi.p2 = set_roi.getSize().getHeight();
        roi.pbin = bin.getY();

        // calculate ROI on the full image size !!
        // binning will be applied on the ROI.
        // Bliss sends already the ROI coordinates with applied binning!
        roi.s1 = roi.s1 * roi.sbin;
        roi.s2 = roi.s2 * roi.sbin;
        roi.p1 = roi.p1 * roi.pbin;
        roi.p2 = roi.p2 * roi.pbin;
    }
    
    // In Lima values are set as starting points x/y and width/height
    // PVCAM requires to have starging x/y and ending x/y
    roi.p2 = roi.p1 + roi.p2 - 1;
    roi.s2 = roi.s1 + roi.s2 - 1;
    ctx->region = roi;

  } catch (Exception e) {
    try {
      if (r.isEmpty()) {
        // if roi is empty set to full frame
        r = limaFullFrameRoi;
      }
      rgn_type roi;
      roi.s1 = r.getTopLeft().x;
      roi.s2 = r.getSize().getHeight();
      roi.sbin = bin.getX();
      roi.p1 = r.getTopLeft().y;
      roi.p2 = r.getSize().getWidth();
      roi.pbin = bin.getY();
      ctx->region = roi;
    } catch (Exception e) {
      _printError("Cannot rollback roi");
    }
    _printError("Cannot set roi rolledback");
  }
};

void Camera::getRoi(Roi& hw_roi) const {
  DEB_MEMBER_FUNCT();
  rgn_type roi = ctx->region;

  Roi r(roi.s1, roi.p1, roi.s2, roi.p2);
  hw_roi = r;

  DEB_PARAM() << DEB_VAR1(hw_roi);
};

void Camera::checkRoi(const Roi& set_roi, Roi& hw_roi) {
  DEB_MEMBER_FUNCT();

  int w = set_roi.getSize().getWidth();
  int h = set_roi.getSize().getHeight();
  int x = set_roi.getTopLeft().x;
  int y = set_roi.getTopLeft().y;

  // if roi area is equal to 0, set to full frame
  if (!set_roi.isActive()) {
    uns16 maxWidth = ctx->sensorResX;
    uns16 maxHeight = ctx->sensorResY;

    w = maxWidth;
    h = maxHeight;
  }

  const Roi new_roi(x, y, w, h);
  hw_roi = new_roi;

  DEB_RETURN() << DEB_VAR1(hw_roi);
};


//-----------------------------------------------------
// Binning functions
//-----------------------------------------------------

void Camera::setBin(const Bin &_bin) {
  DEB_MEMBER_FUNCT();

  ctx->region.sbin = _bin.getX();
  ctx->region.pbin = _bin.getY();
}

void Camera::getBin(Bin &_bin) const {
  DEB_MEMBER_FUNCT();

  _bin = Bin(ctx->region.sbin, ctx->region.pbin);
  DEB_RETURN() << DEB_VAR1(_bin);
}


void Camera::checkBin(Bin &aBin) {
  DEB_MEMBER_FUNCT();

  int x = aBin.getX();
  int y = aBin.getY();

  NVPC binsSer, binsPar;

  if (!ReadEnumeration(ctx->hcam, &binsSer, PARAM_BINNING_SER, "PARAM_BINNING_SER")) {
    _printError("Cannot get available ser serial factors");
  } else {
    uns32 serBinCount = binsSer.size();
    // check if provided bining values are available
    // NVP containers for serial and parallel bining factors should be the same.
    uns32 n;
    for (n=0; n<serBinCount; n++) {
      if (binsSer[n].value == x)
        break;
    }

    uns32 serBin;
    if (n >= serBinCount) {
      // set to maximum possible value if provided value is not available;
      if (PV_OK != pl_get_param(ctx->hcam, PARAM_BINNING_SER, ATTR_MAX, (void*)&serBin))
        _printError("Cannot get max value of ser binning");
    } else {
      serBin = binsSer[n].value;
    }

    x = serBin;
  }

  if (!ReadEnumeration(ctx->hcam, &binsPar, PARAM_BINNING_PAR, "PARAM_BINNING_PAR")) {
    _printError("Cannot get available bin parallel factors error");
  } else {
    uns32 parBinCount = binsPar.size();
    // check if provided bining values are available
    // NVP containers for serial and parallel bining factors should be the same.
    uns32 n;
    for (n=0; n<parBinCount; n++) {
      if (binsPar[n].value == y)
        break;
    }

    uns32 parBin;
    if (n >= parBinCount) {
      // set to maximum possible value if provided value is not available;
      if (PV_OK != pl_get_param(ctx->hcam, PARAM_BINNING_PAR, ATTR_MAX, (void*)&parBin))
        _printError("Cannot get max value of par binning");
    } else {
      parBin = binsPar[n].value;
    }

    y = parBin;
  }

  aBin = Bin(x,y);
}

void Camera::getFrameRate(double& frame_rate) const {
  DEB_MEMBER_FUNCT();
  uns32 readoutTime;

   if (m_wait_flag && IsParamAvailable(ctx->hcam, PARAM_READOUT_TIME, "PARAM_READOUT_TIME")) {
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_READOUT_TIME, ATTR_CURRENT, (void*)& readoutTime))
    {
        _printError();
    }
    // readout param equals to time needed to take image from the sensor. In microseconds
    // division by 10^-6 done to calculate frame rate in Hz
    double fRate = 1. / (m_exp_time + (readoutTime / 1000000.));

    frame_rate = fRate;

    DEB_RETURN() << DEB_VAR1(frame_rate);
   }
}

void Camera::setLatTime(double lat_time){
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(lat_time);

  m_lat_time = lat_time;
}

void Camera::getLatTime(double &lat_time) const{
  DEB_MEMBER_FUNCT();
  lat_time = m_lat_time;

  DEB_RETURN() << DEB_VAR1(lat_time);
}

void Camera::getLatTimeRange(double& min_expo, double& max_expo) const {
  DEB_MEMBER_FUNCT();
  // TODO: needed? How to read from camera?
  min_expo = 0.;
  max_expo = 10.;
};

void Camera::getPixelSize(double& x_pixel_size, double& y_pixel_size) const {
  DEB_MEMBER_FUNCT();
  uns16 _x_pixel_size, _y_pixel_size;

   if (IsParamAvailable(ctx->hcam, PARAM_PIX_PAR_SIZE, "PARAM_PIX_PAR_SIZE")) {
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_PIX_PAR_SIZE, ATTR_CURRENT, (void*)& _x_pixel_size))
    {
        _printError("Error getting pixel parallel size");
    } else {
      if (IsParamAvailable(ctx->hcam, PARAM_PIX_SER_SIZE, "PARAM_PIX_SER_SIZE")) {
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_PIX_SER_SIZE, ATTR_CURRENT, (void*)& _y_pixel_size))
        {
            _printError("Error getting pixel serial size");
        }
      } else {
        _printError("Param Y not available");
      }
    }
  } else {
    _printError("Param X not available");
  }

  // TODO: check if both sizes are ok
  
  x_pixel_size = _x_pixel_size * 1e-9f;
  y_pixel_size = _y_pixel_size * 1e-9f;
}

void Camera::getReadoutTime(double& readout) const {
  DEB_MEMBER_FUNCT();
  readout = m_readoutTime;
  DEB_RETURN() << DEB_VAR1(m_readoutTime);
};

void Camera::_printError(const std::string message) const
{
    DEB_MEMBER_FUNCT();
    int16 errorCode = pl_error_code();
    char pvcamErrMsg[ERROR_MSG_LEN];
    std::string _msg = message;
    if (errorCode) {
      pl_error_message(errorCode, pvcamErrMsg);
      _msg = "Error code: " + std::to_string(errorCode) + ", " + pvcamErrMsg;
      if (!message.empty()) {
        _msg += " , " + message;
      }
    }

    THROW_HW_ERROR(Error) << _msg;
}
