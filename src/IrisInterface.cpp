//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "iris/Interface.h"
#include "iris/BinCtrlObj.h"
#include "iris/RoiCtrlObj.h"
#include "iris/ShutterCtrlObj.h"
// #include "iris/VideoCtrlObj.h"
#include "iris/Camera.h"

using namespace lima;
using namespace lima::Iris;

Interface::Interface(Camera &cam) : m_cam(cam)
{
  // Cap list
  m_det_info = new DetInfoCtrlObj(cam);
  m_sync = new SyncCtrlObj(cam);
  m_roi = new RoiCtrlObj(cam);
  m_bin = new BinCtrlObj(cam);

//   bool has_video_capability;
//   bool force_video_mode = false;
//   m_cam.hasVideoCapability(has_video_capability);
//   printf("1__");

//   if (has_video_capability || force_video_mode) {
//     printf("2__");
//     if (!force_video_mode) {
//       printf("3__");
//       m_cam._forceVideoMode(true);
//     }
//     m_video = new VideoCtrlObj(cam);
//   } else
//     printf("4__");
//     m_video = NULL;
}

Interface::~Interface()
{
  DEB_DESTRUCTOR();
  delete m_det_info;
  delete m_sync;
  delete m_roi;
  delete m_bin;
}

void Interface::getCapList(CapList &cap_list) const
{
  cap_list.push_back(HwCap(m_det_info));

  HwBufferCtrlObj *buffer = m_cam.getBufferCtrlObj();
  cap_list.push_back(HwCap(buffer));

  cap_list.push_back(HwCap(m_sync));
  cap_list.push_back(HwCap(m_roi));
  cap_list.push_back(HwCap(m_bin));
}

void Interface::reset(ResetLevel reset_level)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(reset_level);

  stopAcq();
  m_cam._setStatus(Camera::Ready, true);
}

void Interface::prepareAcq()
{
  DEB_MEMBER_FUNCT();
  m_cam.prepareAcq();
}

void Interface::startAcq()
{
  DEB_MEMBER_FUNCT();
  m_cam.startAcq();
}

void Interface::stopAcq()
{
  DEB_MEMBER_FUNCT();

  m_cam.stopAcq();
}

int Interface::getNbHwAcquiredFrames()
{
  DEB_MEMBER_FUNCT();
  int acq_frames;
  m_cam.getNbHwAcquiredFrames(acq_frames);
  return acq_frames;
}

void Interface::getStatus(StatusType& status) {
  DEB_MEMBER_FUNCT();
  Camera::Status iris_status = Camera::Ready;

  m_cam.getStatus(iris_status);
  switch (iris_status)
  {
    case Camera::Ready:
      status.set(HwInterface::StatusType::Ready);
      break;
    case Camera::Exposure:
      status.set(HwInterface::StatusType::Exposure);
      break;
    case Camera::Readout:
      status.set(HwInterface::StatusType::Readout);
      break;
    case Camera::Latency:
      status.set(HwInterface::StatusType::Latency);
      break;
    case Camera::Fault:
      status.set(HwInterface::StatusType::Fault);
  }
}

void Interface::getFrameRate(double& frame_rate) {
  m_cam.getFrameRate(frame_rate);
}
