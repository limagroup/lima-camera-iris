#include "iris/Camera.h"
#include "iris/RoiCtrlObj.h"

using namespace lima;
using namespace lima::Iris;

RoiCtrlObj::RoiCtrlObj(Camera& cam) : m_cam(cam) {
    DEB_CONSTRUCTOR();
}

RoiCtrlObj::~RoiCtrlObj() {
    DEB_DESTRUCTOR();
}

void RoiCtrlObj::checkRoi(const Roi& set_roi, Roi& hw_roi) {
    DEB_MEMBER_FUNCT();
    m_cam.checkRoi(set_roi, hw_roi);
}

void RoiCtrlObj::setRoi(const Roi& set_roi) {
    DEB_MEMBER_FUNCT();
    Roi real_roi;
    checkRoi(set_roi, real_roi);
    m_cam.setRoi(real_roi);
}

void RoiCtrlObj::getRoi(Roi& hw_roi) {
    DEB_MEMBER_FUNCT();
    m_cam.getRoi(hw_roi);
}

