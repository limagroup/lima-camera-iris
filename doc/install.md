# Instalation


## Linux

### System and drivers

As a regular user with sudo (not blissadm, not root):

```
$ cd $HOME
$ cp PVCAM-Linux-3-9-11-2.zip .
$ unzip PVCAM-Linux-3-9-11-2.zip

$ cd pvcam
$ bash pvcam__install_helper-Ubuntu.sh
```

should give

```
Verifying archive integrity... All good.
Uncompressing pvcam_3.9.11.2 for Linux by Teledyne Photometrics  100%
```

```
Do you want to set the limit automatically via Cron on every boot? [Y/n]: n
```

> Ensure the user is a member of group 'users' to work with cameras without super-user rights.
> To do that use e.g. following command (for user john):
```
   sudo usermod -a -G users john
```


About to install following packages:
    libwxgtk3.0-gtk3-0v5
Do you want to continue? [Y/n]: y

About to add user 'debionne' to group 'users'.
Do you want to continue? [Y/n]: n


$ sudo usermod -a -G users opid00

For all the kernels versions installed:

```
pvcam_pcie.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.4.0-135-generic/updates/dkms/

depmod...

DKMS: install completed.
(Re)loading PCIE driver
```

```
$ lsmod | grep pvcam
pvcam_pcie             65536  0
```
