.. _camera-iris:

Iris
--------

.. image:: iris15.png
.. image:: kinetix.png

Introduction
````````````

This is the official Lima camera iris. It has been made to help you getting started with Lima and to test/play Lima without any hardware.

Prerequisite
````````````

The Teledyne PVCAM SDK version 3.9 or upper should be installed on your computer where PCIe and/or USB3 drivers will be installed.
The PVCAM SDK are available here: https://www.photometrics.com/support/software-and-drivers


Installation & Module configuration
````````````````````````````````````

Follow the generic instructions in :ref:`build_installation`. If using CMake directly, add the following flag:

.. code-block:: sh

  -DLIMACAMERA_IRIS=true

For the Tango server installation, refers to :ref:`tango_installation`.

Initialisation and Capabilities
```````````````````````````````

Implementing a new plugin for new detector is driven by the LIMA framework but the developer has some freedoms to choose which standard and specific features will be made available. This section is supposed to give you the correct information regarding how the camera is exported within the LIMA framework.

Camera initialisation
.....................

.. cpp:namespace-push:: lima::iris

The camera will be initialized within the :cpp:class:`Camera` object. The :cpp:func:`Camera` constructor takes an optional mode parameter.

Standard capabilities
.....................

This plugin has been implemented in respect of the mandatory capabilites but with some limitations which are due to the camera. We only provide here extra information for a better understanding of the capabilities for Ximea cameras.

* HwDetInfo

* HwSync

  get/setTrigMode():  supported modes are IntTrig, ExtTrigSingle and ExtTrigMult.

Optional capabilities
.....................

In addition to the standard capabilities, we make the choice to implement some optional capabilities which
are supported by this detector. A Shutter control.

* HwBin

  Supported modes: 1x1, 2x2, 4x4

* HwRoi
  Any Roi size are supported


How to use
```````````````

This is a python code example of a simple acquisition:

.. code-block:: python

  import time
  from Lima import Core, Iris

  cam = Iris.Camera("pvcamPCIE_0", "")
  hw = Iris.Interface(cam)
  ct = Core.CtControl(hw)

  # configure saving
  sav = ct.saving()
  sav.setSavingMode(Core.CtSaving.AutoFrame)
  sav.setFormat(Core.CtSaving.HDF5BS)
  sav.setPrefix('test')
  sav.setOverwritePolicy(Core.CtSaving.Overwrite)
  sav.setDirectory('/tmp')

  # set configuration, see documentation for details
  ct.image().setBin(Core.Bin(2, 2))

  ct.prepareAcq()
  ct.startAcq()

  while ct.getStatus().AcquisitionStatus != Core.AcqReady:
      time.sleep(0.1)

  img = ct.ReadBaseImage(0)
