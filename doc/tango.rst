Iris Tango device
==================

This is the reference documentation of the Iris Tango device.

You can also find some useful information about the camera models/prerequisite/installation/configuration/compilation in the :ref:`Iris camera plugin <camera-iris>` section.


Properties
----------

========================= ========== ========================================== ====================================================
Property name             Mandatory  Default value                              Description
========================= ========== ========================================== ====================================================
camera_name               No*        N/A                                        Camera ID (e.g pvcamPCIE_0)
serial_number             No*        N/A                                        Camera serial number
========================= ========== ========================================== ====================================================
(*) at least one of those properties should be set up, otherwise the camera will be not detected.

Attributes
----------

============================= ==== ========== ================================================================
Attribute name                RW   Type       Description
============================= ==== ========== ================================================================
acq_timeout                   rw   DevDouble  Acquisition timeout in second
readout_time                  r    DevBoolean Sensor readouttime in us
temperature                   r    DevDouble  Sensor temperature in celsuis
============================= ==== ========== ================================================================


Commands
--------

=======================	=============== ================== ============================================
Command name            Arg. in         Arg. out           Description
=======================	=============== ================== ============================================
Init                    DevVoid         DevVoid	           Do not use
State                   DevVoid         DevLong            Return the device state
Status                  DevVoid         DevString          Return the device state as a string
getAttrStringValueList  DevString:      DevVarStringArray: Return the authorized string value list for
                        Attribute name  String value list  a given attribute name
=======================	=============== ================== ============================================
